<?php
	get_header();
?>

<div class="container content">
	<?php if ( has_post_thumbnail() ) { ?>
	<div class='split-pane col-xs-12 col-sm-6 photo-side' style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	</div>
	<?php } else { ?>
	<div class='split-pane col-xs-12 col-sm-6 photo-side' style="background-image: url('http://mixed-drinks/wp-content/uploads/2018/01/placeholder_drink.jpg');">
	</div>
	<?php }?>
	<div class='split-pane col-xs-12 col-sm-6 text-side'>
		<?php if (have_posts()) :
   					while (have_posts()) :
      				the_post();
         				the_content();
   					endwhile;
					endif; ?>
					<?php

					$fields = get_field_objects();

					if( $fields )
					{
						foreach( $fields as $field_name => $field )
						{
							echo '<div>';
								echo '<h3>' . $field['label'] . '</h3>';
								$values = $field['value'];
								foreach ($values as $key => $value) {
									echo '<p class="'.$field['label'].' '.$value->post_title.'">'.$value->post_title.'</p>';
								}
							echo '</div>';
						}
					} ?>
	</div>

</div>

<?php
	get_footer();
?>
