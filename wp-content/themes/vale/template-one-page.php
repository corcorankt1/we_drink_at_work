<?php

	/*
	Template Name: One Page
	*/

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
   
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.2, user-scalable=yes" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php 
	
	do_action(  suevafree_setting( 'vale_header_layout', 'vale_header_layout'), 'one-page-menu', 'one-page-menu' );
	do_action( 'suevafree_onepage_sidebar', 'onepage-sidebar-area');
	get_footer(); 
	
?>