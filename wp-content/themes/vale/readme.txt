Vale Wordpress Theme
---------------------

Vale is the new SuevaFree child theme, optimized to work with WooCommerce e-commerce plugin. Vale WordPress theme offers a new header layout, in addition to the default five header layouts and new Google Fonts. SuevaFree Essential Kit plugin is required to enable the one page section, available from WordPress.org

Created by ThemeinProgress, http://www.themeinprogress.com

License
-------
Vale is licensed under the GPL 3.0 license.

Credits
-------

/** IMAGES **/

- Screenshot images

-- By Pexels - https://www.pexels.com

-- https://www.pexels.com/photo/sunset-ocean-pier-seashore-91880/ by Josh Sorenson ( https://www.pexels.com/u/joshsorenson/ )
-- https://www.pexels.com/photo/green-field-and-brown-and-white-house-during-sunrise-677845/ by Oscar Mikols ( https://www.pexels.com/u/oscar-mikols-152204/ )
-- https://www.pexels.com/photo/adult-beard-countryside-guitar-1543/ by SplitShire ( https://www.splitshire.com/guitar-player/ )

--- All images are licensed under CC0 License.

- Patters

-- By Theme in Progress - http://www.themeinprogress.com

--- Licensed under GNU General Public License v3.

/** ICONS **/

- Font Awesome

-- By Dave Gandy - http://fortawesome.github.io/Font-Awesome/

--- Font License under SIL OFL 1.1 - http://scripts.sil.org/OFL ( Applies to all desktop and webfont files in the following directory: /sueva/fonts/ )
--- Code License under MIT License - http://opensource.org/licenses/mit-license.html ( Applies to the font-awesome.min.css file in /sueva/css/ )
--- Brand Icons - All brand icons are trademarks of their respective owners. The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.

/** FONTS **/

- Google Web Fonts (Dr Sugiyama, Libre Franklin)

-- By Google - http://google.com

--- Dr Sugiyama, Font by Sudtipos, Licensed under Open Font License
https://fonts.google.com/specimen/Dr+Sugiyama

--- Libre Franklin, Font by Impallari Type, Licensed under Apache License, Version 2.0
https://fonts.google.com/specimen/Libre+Franklin