<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.2, user-scalable=yes" />

<!-- FONT AWESOME CDN -->
<script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>


<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php do_action( suevafree_setting( 'vale_header_layout', 'vale_header_layout'), 'main-menu', 'default-menu' ); ?>
