<?php get_header(); ?>
<section class="w-90 pv4 pl4 flex">
  <div class="w-third">
    <?php get_search_form(); ?>
  </div>
</section>
<section class="flex w-100 flex-wrap justify-evenly">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'entry' ); ?>
  <?php endwhile; endif; ?>
  <?php echo do_shortcode('[ajax_load_more id="8350738841" css_classes="flex w-100 flex-wrap justify-evenly" post_type="post" posts_per_page="12" offset="12" transition="slide" transition_container="false" button_label="More"]');?>
</section>

<?php get_footer(); ?>
