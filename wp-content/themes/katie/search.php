<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : ?>
<section class="w-90 pv4 pl4 flex">
  <div class="w-third">
    <?php get_search_form(); ?>
  </div>
</section>
<h1 class="pl4"><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h1>
<section class="flex w-100 flex-wrap justify-evenly">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php get_template_part( 'entry' ); ?>
  <?php endwhile; endif; ?>
</section>
<?php else : ?>
<article id="" class="">
<header class="">
<h2 class="pl4"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
</header>
<section class="pl4">
<p class="proxima_regular pl4"><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
<section class="w-90 pv4 pl4 flex">
  <div class="w-third">
    <?php get_search_form(); ?>
  </div>
</section>
</section>
</article>
<?php endif; ?>
</section>

<?php get_footer(); ?>
