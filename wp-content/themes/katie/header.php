<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112824710-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-112824710-1');
</script>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width" />
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main.css" />
  <!-- CSS -->
  <!-- FONT AWESOME 5-->
  <script>
  FontAwesomeConfig = { searchPseudoElements: true };
  </script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <?php wp_head(); ?>
  <!-- FONT AWESOME 5-->

  <!-- GOOGLE FONT PLAYFAIR DISPLAY -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,900" rel="stylesheet">

</head>
<body <?php body_class(); ?>>
  <header class='w-100 flex flex-wrap-mobile'>
    <button id="toggleNav" class="bg-red font_5 white proxima_medium w-10 pa2 text_caps flex pv3 ph4 b--none z-100 justify-center flex-wrap-mobile">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/menu.png" class='mr3'>
      menu
    </button>
    <nav id="nav" style="z-index:90;" class="absolute">
      <div class="fixed bg-red white font_3 w-40 flex">
        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
      </div>
    </nav>
    <h1  class="ma0 pl3 flex w-40 justify-start"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home" class="text_dec_none black dim playfair_display" style="font-size:24px;padding-top:.5em;"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a></h1>
    <div class="flex justify-end w-50 items-center pr4 z-100">
      <!-- <a href="#" class="red font_4 mh2 dim"><i class="fab fa-facebook-f"></i></a>
      <a href="#" class="red font_4 mh2 dim"><i class="fab fa-twitter"></i></a> -->
    </ul>
  </header>
