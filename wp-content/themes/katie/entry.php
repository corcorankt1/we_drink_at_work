<article id="post-<?php the_ID(); ?>" class="grid-item mh1 mb5">
  <?php if ( has_post_thumbnail() ) { ?>
    <a href="<?php the_permalink(); ?>"><div style="background-image:url(<?php the_post_thumbnail_url('medium');?>); height:336px;width:336px;background-repeat:no-repeat;background-size:cover;background-position:center center;"></div> </a>
   <?php } ?>
  <header class="text_left">
    <h1 class="font_2 playfair_display ma0 pt2"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" class="black text_dec_none dim"><?php the_title(); ?></a></h1>
  </header>
  <section class="flex w-100 flex-wrap">
    <?php the_category(); ?>
    <ul class="ma0 pl3">
      <li class="gray font_5 proxima_medium ma0 pt1"><?php the_time( get_option( 'date_format' ) ); ?></li>
    </ul>
  </section>
</article>
