<?php get_header(); ?>
<section class="w-90 pv4 pl4 flex">
  <div class="w-third">
    <?php get_search_form(); ?>
  </div>
</section>
<h1 class="pl4"><?php _e( 'Category Archives: ', 'blankslate' ); ?><?php single_cat_title(); ?></h1>
<?php if ( '' != category_description() ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . category_description() . '</div>' ); ?>
<section class="flex w-100 flex-wrap justify-evenly">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php get_template_part( 'entry' ); ?>
  <?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>
