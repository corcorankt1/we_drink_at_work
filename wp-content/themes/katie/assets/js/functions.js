// TOGGLE NAVIGATION
const navButton = document.querySelector('#toggleNav');
const nav = document.querySelector('#nav');
navButton.addEventListener('click', () => nav.classList.toggle('open'));
