
<footer class="bg-black flex items-center h-6 mt5">
<p class="font_6 white proxima_light_it ma0 pl3 ml2"><?php echo sprintf( __( '%1$s %2$s %3$s', 'blankslate' ), 'Copyright &copy;', date( 'Y' ), '. We Drink at Work' ); ?></p>
</footer>
<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/functions.js"></script>
</body>
</html>
