<?php
	get_header();
?>

<section class="flex flex-wrap w-100 pa0">
  <div class="w-40 h-100 pa4 mt4 flex flex-column" style="min-height:70vh;">
    <?php $url = home_url(); ?>
    <?php if (have_posts()) :
            while (have_posts()) :
              the_post(); ?>
              <h1 class="playfair_display font_1 black mt2 mb0"><?php the_title(); ?></h1>
               <?php the_category(); ?>
              <?php the_content(); ?>
            <?php endwhile;
          endif; ?>
          <?php

          $fields = get_field_objects();

          if( $fields )
          {
            foreach( $fields as $field_name => $field )
            {
              echo '<div>';
                echo '<h3>' . $field['label'] . '</h3>';
                $values = $field['value'];
                foreach ($values as $key => $value) {
                  echo '<p class="'.$field['label'].' '.$value->post_title.'">'.$value->post_title.'</p>';
                }
              echo '</div>';
            }
          } ?>
  </div>
  <?php if ( has_post_thumbnail() ) { ?>
  <div class="w-50 h-100 fixed bg-cover bg-no-repeat left-50 top-0" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  </div>
	<!-- <div class="gradient w-50 h-third top-two-thirds fixed left-50 o-90 z-100"></div>
	<div class="w-50 h-third top-two-thirds fixed bg-cover bg-center bg-no-repeat left-50" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	</div> -->
  <?php } else { ?>
    <div class="w-50 h-100 fixed bg-cover bg-no-repeat left-50 top-0" style="background-image: url('http://mixed-drinks/wp-content/uploads/2018/01/placeholder_drink.jpg');">
    </div>
		<!-- <div class="w-50 h-third top-two-thirds fixed bg-cover bg-center bg-no-repeat left-50" style="background-image: url('http://mixed-drinks/wp-content/uploads/2018/01/placeholder_drink.jpg');">
		</div> -->
    <?php } ?>

</section>



<?php
	get_footer();
?>