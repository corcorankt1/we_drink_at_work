var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css')
var sass = require('gulp-ruby-sass');
var babel = require("gulp-babel");
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task("babel", function () {
  return gulp.src("js/app.js")
    .pipe(babel())
    .pipe(gulp.dest("js/babel"));
});
gulp.task('compress', function (cb) {
  pump([
        gulp.src('js/babel/*.js'),
        uglify(),
        gulp.dest('js/output')
    ],
    cb
  );
});
gulp.task('sass', () =>
    sass('css/styles.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('css'))
);
gulp.task('autoprefixer',function(){
  gulp.src('css/styles.css')
    .pipe(autoprefixer())
    .pipe(gulp.dest('css/autoprefix'))
});
gulp.task('cleanCSS',function(){
  gulp.src('css/styles.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('css/minified'))
});
gulp.task('outputCSS',function(){
    gulp.src('css/styles.css')
    .pipe(autoprefixer())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('css/output'))
});
